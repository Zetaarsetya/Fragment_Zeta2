package id.sch.smktelkom_mlg.learn.fragment2_zeta.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import id.sch.smktelkom_mlg.learn.fragment2_zeta.R;

/**
 * Created by Zeta-PC on 01/03/2018.
 */

public class KoalaFragment extends Fragment {
    public KoalaFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_multiple_koala,
                container, false);
        return rootView;
    }

}
